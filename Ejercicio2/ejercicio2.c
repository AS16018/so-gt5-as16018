#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
/*
la llamada al sistema execve sustituye la imagen del
proceso que la llamó por una nueva imagen ejecutable dada,
es decir, el proceso que llamó a execve se transforma en un
nuevo proceso.

path: el contenido de path debe ser la ruta del archivo
ejecutable que sustituirá a la imagen del proceso que llamo
a execve.

argv: es una lista de argumentos que se le enviaran al nuevo
proceso.

envp: es una lista de variables de entorno que se enviaran al
nuevo proceso.
*/
int main() {

  int pid, status;
  char *argumentos[] = {"cp", "directorio_1/archivo_a_copiar", "directorio_2"};
  char *env[] = {"TERM=xterm"};
  pid = fork();
  if (pid == -1) {
    printf("Error en fork\n");
    exit(-1);
  }else if (pid == 0) {
    execve("/usr/bin/cp", argumentos, env);
    perror("Error de exec, en la llamada al sistema");
    exit(-1);
  }else{
    while (wait(&status) != pid);
    (status == 0)?printf("Ejecución normal de hijo\nFuncion de comando cp Exitosa!\ncomo ejemplo Se copió 'archivo_a_copiar' de directorio_1 a directorio_2\n"):printf("Error en el hijo\n");
  }

  return 0;
}
